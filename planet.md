# Name of Planet #

Description/observations and other things to note here.

<code=markdown>
| **Uploaded Name** | {} () |||
| **Original Name** | |||
| **Type** | |||
| **Weather** | |||
| **Sentinels** | |||
| **Flora** | |||
| **Fauna** | | ✔ ✘ ||
^  **Atmosphere Conditions**  ^^^^
| | //°C// | //rad// | //tox// |
| **Day** | | | |
| **Night** | | | |
| **Cave** | | | |
| **Water** | | | |
| **Storm** | | | |
</code>

<code=markdown>
^  **Harvest**  ^^^^^
| **Common** | antrium ✔ | carbon ✔ | heridium ✔ | iron ✔ |
| **Uncommon** | copper | iridium | nickel | platinum ✔ |
| | thamium9 ✔ | zinc | | |
| **Rare** | aluminium | candensium | chrysonite | coryzagen |
| | cymatygen | emeril | fervidium | gold |
| | plutonium ✔ | rigogen | rubeum | spadonium |
| | temerium | titanium | viridium | |
| **Very Rare** | calium | murrine | omegon | radnox |
| **Commodities** | **albumen pearl** | aquasphere | **gravitino ball** | **sac venom** |
| | vortex cube | | |
</code>

<code=markdown>
|  **Portal**  | ✔ ✘ |
</code>