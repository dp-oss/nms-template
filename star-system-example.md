# Morongjumin-Igau #

<code=markdown>
| **Mode** | Normal ||
| **System** | ||
| **Galaxy** | Euclid ||
| **Discovered** | 12/31/2016 23:03 ||
| **Star Color** | Yellow ||
| **Uploaded Name** | [G-66] TTPS 034 (CoFrTe) ||
| **Original Name** | Morongjumin-Igau ||
| **Region** | Bleiousanub Void ||
| **Analysis** | Class G7 ||
| | 4 planets | 2 moons |
| **Distance (Ly)** | 131,831.1 ||
| **Last System (Ly)** | 1,670.1 ||
| **Last System** | TTPS 033 ||
| **Coordinates** | 0573:0082:0385:0066 ||
| **Faction** | Gek ||
| **Space** | Dark red-blue ||
</code>

<code=markdown>
^  **Asteroids**  ^^^^
| copper ✔ | emeril | gold | iridium |
| iron ✔ | nickel ✔ | thamium9 ✔ |
</code>

<code=markdown>
^  **Trade Terminal**  ^^^
| //Item// | //Sell// | //Base Sell// |
| Dynamic Resonator ★ | 53,610.6 (+94.9) | |
| Gravitino Ball ★ | 16,406.4 (+98.9) | |
| Lubricant | **38,422.1 (-1.7)** | 37,965.3 (-2.9) |
| Explosive | **40,257.3 (+2.9)** | 39,638.3 (+1.4) |
| Glass | **29,662.2 (-1.2)** | 29,189.3 (-2.8) |
| Acid | **39,978.2 (+2.2)** | 38,008.4 (-2.8) |
| Non-Ferrous Plate | **30,644.3 (-0.9)** | 30,125.2 (-2.6) |
| Insulating Gel | **39,894.5 (+2.0)** | 38,315.5 (-2.0) |
| Poly-Fibre | **34,388.5 (-3.5)** | 33,833.6 (-5.1) |
</code>