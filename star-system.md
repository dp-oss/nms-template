# Name of System #
<code=markdown>
| **Mode** | Normal ||
| **System** | ||
| **Galaxy** | Euclid ||
| **Discovered** | MM/DD/YYYY HH:MM ||
| **Star Color** | ||
| **Uploaded Name** | ||
| **Original Name** | ||
| **Region** | ||
| **Analysis** | ||
| | planets | moons |
| **Distance (Ly)** | ||
| **Last System (Ly)** | ||
| **Last System** | ||
| **Coordinates** | ||
| **Faction** | ||
| **Space** | ||</code>

<code=markdown>
^  **Asteroids**  ^^^^
| copper | emeril | gold | iridium |
| iron ✔ | nickel | thamium9 ✔ |</code>

<code=markdown>
^  **Trade Terminal**  ^^^
| //Item// | //Sell// | //Base Sell// |
| Dynamic Resonator ★ | | |
| ★ | | |
| Lubricant | | |
| Explosive | | |
| Glass | | |
| Acid | | |
| Non-Ferrous Plate | | |
| Insulating Gel | | |
| Poly-Fibre | | |
</code>