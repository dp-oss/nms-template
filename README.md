Using dokuwiki markdown syntax for the tables, so be sure to check out the raw
source of these templates.

Feel free to adapt to your needs.

#### Star System ####

![](./images/wiki-sys01.png)

#### Planet / Moon ####

![](./images/wiki-sys02.png)
