# Hoot Dawgy (Empty) #

There are worse gas station planets. This one at least has nice temperatures during the day and water with the occasional aquasphere.

<code=markdown>
| **Uploaded Name** | Hoot Dawgy (Empty) |||
| **Original Name** | Zuppyodharuw |||
| **Type** | empty planet |||
| **Weather** | emollient |||
| **Sentinels** | standard |||
| **Flora** | deficient |||
| **Fauna** | none | 0 ✔ ||
^  **Atmosphere Conditions**  ^^^^
| | //°C// | //rad// | //tox// |
| **Day** | 1.8 | 1.8 | 14.9 |
| **Night** | 26.6 | 1.8 | 15.1 |
| **Cave** | 27.7 | 1.3 | 2.8 |
| **Water** | 12.7 | 0.8 | 9.1 |
| **Storm** | | | |
</code>

<code=markdown>
^  **Harvest**  ^^^^^
| **Common** | antrium ✔ | carbon ✔ | heridium ✔ | iron ✔ |
| **Uncommon** | copper | iridium | nickel ✔ | platinum ✔ |
| | thamium9 ✔ | zinc | | |
| **Rare** | aluminium | candensium | chrysonite | coryzagen |
| | cymatygen | emeril | fervidium | gold ✔ |
| | plutonium ✔ | rigogen | rubeum | spadonium |
| | temerium | titanium | viridium | |
| **Very Rare** | calium | murrine | omegon | radnox |
| **Commodities** | **albumen pearl** | aquasphere ✔ | **gravitino ball** | **sac venom** |
| | vortex cube | | |
</code>

<code=markdown>
|  **Portal**  | ✔ ✘ |
</code>